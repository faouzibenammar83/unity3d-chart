﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GraphicDescription : MonoBehaviour {

	public RectTransform extremeValueLinePrefab;
	public RectTransform extremeValueDescriptionPrefab;
	public RectTransform mediumValueLinePrefab;
	public RectTransform mediumValueDescriptionPrefab;
	public RectTransform quarterValueLinePrefab;
	public RectTransform quarterValueDescriptionPrefab;
	public RectTransform intermediumValueLinePrefab;
	public RectTransform intermediumValueDescriptionPrefab;

	public int intermediumSplit;//each quarter

	private Text minumumValueDescriptionText;
	private Text maximumValueDescriptionText;
	private Text mediumValueDescriptionText;
	private Text smallQuarterValueDescriptionText;
	private Text bigQuarterValueDescriptionText;

	private List<RectTransform> ordinateList = new List<RectTransform>();
	private List<RectTransform> abscissaeList = new List<RectTransform>();
	
	private IGraphic graphicInterface;

	
	void Start () {
		graphicInterface = (IGraphic)GetComponent (typeof(IGraphic));

		ordinateList.Add(MakeDescription (Vector2.zero, extremeValueDescriptionPrefab, extremeValueLinePrefab));
		ordinateList.Add(MakeDescription (new Vector2(1.0f, 0), extremeValueDescriptionPrefab, extremeValueLinePrefab));
		ordinateList.Add(MakeDescription (new Vector2(0.5f, 0), mediumValueDescriptionPrefab, mediumValueLinePrefab));
		ordinateList.Add(MakeDescription (new Vector2(0.25f, 0), quarterValueDescriptionPrefab, quarterValueLinePrefab));
		ordinateList.Add(MakeDescription (new Vector2(0.75f, 0), quarterValueDescriptionPrefab, quarterValueLinePrefab));

		for (float quarter = 0 ; quarter < 1 ; quarter += 0.25f) {
			for (int i = 1; i < intermediumSplit; ++ i) {
				ordinateList.Add(MakeDescription( new Vector2(quarter + (0.25f / intermediumSplit) * i, 0), intermediumValueDescriptionPrefab, intermediumValueLinePrefab));
			}
		}

		abscissaeList.Add(MakeDescription (new Vector2(0, 1.0f), extremeValueDescriptionPrefab, extremeValueLinePrefab));
		abscissaeList.Add(MakeDescription (new Vector2(0, 0.5f), mediumValueDescriptionPrefab, mediumValueLinePrefab));
		abscissaeList.Add(MakeDescription (new Vector2(0, 0.25f), quarterValueDescriptionPrefab, quarterValueLinePrefab));
		abscissaeList.Add(MakeDescription (new Vector2(0, 0.75f), quarterValueDescriptionPrefab, quarterValueLinePrefab));

		for (float quarter = 0 ; quarter < 1 ; quarter += 0.25f) {
			for (int i = 1; i < intermediumSplit; ++ i) {
				abscissaeList.Add(MakeDescription( new Vector2(0, quarter + (0.25f / intermediumSplit) * i), intermediumValueDescriptionPrefab, intermediumValueLinePrefab));
			}
		}

		UpdateGraphic ();
	}

	RectTransform MakeDescription(Vector2 anchored, RectTransform prefab, RectTransform linePrefab) {
		RectTransform line = GameObject.Instantiate(linePrefab) as RectTransform;
		line.transform.SetParent(transform, false);
		line.SetSiblingIndex (0);
		RectTransform inter = GameObject.Instantiate(prefab) as RectTransform;
		inter.transform.SetParent(transform, false);

		inter.anchorMax = inter.anchorMin = anchored;
	
		line.anchorMax = line.anchorMin = inter.anchorMax;

		if (inter.anchorMax.y != 0) {
			line.anchorMax = new Vector2 (1, line.anchorMax.y);
			line.sizeDelta = new Vector2 (0, line.sizeDelta.x);
		} else if (inter.anchorMax.x != 0) {
			line.anchorMax = new Vector2 (line.anchorMax.x, 1);
			line.sizeDelta = new Vector2 (line.sizeDelta.x, 0);
		} else {
			line.anchorMax = new Vector2 (line.anchorMax.x, 1);
			line.sizeDelta = new Vector2 (line.sizeDelta.x, 0);
		}

		return inter;
	}

	void Update() {
		UpdateGraphic ();
	}
	
	void UpdateGraphic() {
		foreach (RectTransform description in ordinateList) {
			float val = graphicInterface.SmallestValue.x + Mathf.Abs(description.anchorMax.x * (graphicInterface.BiggestValue.x - graphicInterface.SmallestValue.x));
			description.GetComponentInChildren<Text>().text = val.ToString("0.0");
		}

		foreach (RectTransform description in abscissaeList) {
			float val = graphicInterface.SmallestValue.y + Mathf.Abs(description.anchorMax.y * (graphicInterface.BiggestValue.y - graphicInterface.SmallestValue.y));
			description.GetComponentInChildren<Text>().text = val.ToString("0.0");
		}
	}
}
