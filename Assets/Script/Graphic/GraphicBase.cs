﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//ao invés de "shrink", poder também pegar um pedaço do gráfico (slice)
public abstract class GraphicBase : MonoBehaviour, IGraphic {

	[SerializeField]
	protected RectTransform representationPrefab;

	[SerializeField]
	protected RectTransform plotter;

	[SerializeField]
	protected List<Vector2> numbers;

	public float margin = 5;



	protected List<RectTransform> representations = new List<RectTransform>();
	private Vector2 biggestValue;
	private Vector2 smallestValue;
	protected Vector2 viewBoundaries;
	protected bool isClipped = false;

	private int ChildIndex;

	public int VizibleNumbersCount {
		get {
			if (!isClipped) {
				return numbers.Count;
			}

			Vector2 range = NumberRangeIndex();
			Debug.Log(range);
			return (int)(range.y - range.x);
		}
	}

	protected Vector2 NumberRangeIndex() {
		Vector2 range = new Vector2(-1, -1);

		SortCoordinates();

		for (int i = 0 ; i < numbers.Count ; ++ i) {
			float smallVal = numbers[i].x;
			float bigVal = numbers[(numbers.Count - 1) - i].x;
			if (smallVal >= viewBoundaries.x && range.x == -1) {
				range.x = i;
			}
			if (bigVal <= viewBoundaries.y && range.y == -1) {
				range.y = numbers.Count - i;
			}
			if (range.x != -1 && range.y != -1) return range;
		}

		return Vector2.zero;
	}
	
	public Vector2 SmallestValue { 
		get {
			if (isClipped) {
				return new Vector2(viewBoundaries.x, smallestValue.y);
			}
			return smallestValue;
		}
	}

	public Vector2 BiggestValue { 
		get {
			if (isClipped) {
				return new Vector2(viewBoundaries.y, biggestValue.y);
			}
			return biggestValue;
		} 
	}

	public void ClipView(float min, float max) {
		isClipped = true;
		viewBoundaries = new Vector2(min, max);
	}

	public void FullView() {
		isClipped = false;
	}

	virtual public void AddCoordinate(Vector2 val) {
		if (val.y > biggestValue.y) {
			biggestValue.y = val.y;
		} 
		if (val.y < smallestValue.y) {
			smallestValue.y = val.y;
		}

		if (val.x > biggestValue.x) {
			biggestValue.x = val.x;
		} 
		if (val.x < smallestValue.x) {
			smallestValue.x = val.x;
		}

		numbers.Add (val);
	}

	public void AddCoordinate(float ordinate, float abscissae) {
		AddCoordinate(new Vector2(ordinate, abscissae));
	}


	virtual public void UpdateGraphic() {
		ChildIndex = 0;

		PreparePool();

		SortCoordinates();

		if (StartPlot()) {
			UpdateGraphic(numbers, Normalization);
		}

		FinishedPlot();
	}


	virtual protected void UpdateGraphic(RectTransform representation, Vector2 val, Vector2 normalization){}
	virtual protected void FinishedPlot(){}
	virtual protected bool StartPlot(){return true;}


	virtual protected void UpdateGraphic(List<Vector2> numbers, Vector2 normalization) {
		if (!isClipped) {
			foreach(Vector2 val in numbers) {
				UpdateGraphic(GetNextRepresentation(), val, normalization);
			}
		} else {
			foreach(Vector2 val in numbers) {
				if (val.x >= viewBoundaries.x && val.x <= viewBoundaries.y) {
					UpdateGraphic(GetNextRepresentation(), val, normalization);
				}
			}
		}
	}

	private void SortCoordinates() {
		numbers.Sort (delegate(Vector2 x, Vector2 y) {
			return x.x.CompareTo (y.x);
		});
	}

	virtual protected Vector2 Normalization {
		get {
			return new Vector2( (BiggestValue.x - SmallestValue.x) / (plotter.rect.size.x - margin * 2),
				(BiggestValue.y - SmallestValue.y) / (plotter.rect.size.y - margin * 2));
		}
	}

	//TODO: Pool might be more smart, disabling GameObjects only if not are used and if it is clipped
	virtual protected void PreparePool() {
		foreach(Transform t in plotter) {
			t.gameObject.SetActive(false);
		}

		for (int index = plotter.childCount ; index < numbers.Count ; ++index) {
			RectTransform representation = GameObject.Instantiate(representationPrefab) as RectTransform;
			representation.transform.SetParent(plotter, false);
			representations.Add(representation);
			representation.gameObject.SetActive(false);
		}
	}

	virtual protected RectTransform GetNextRepresentation() {
		RectTransform t = representations[ChildIndex++];
		t.gameObject.SetActive(true);
		return t;
	}


	void Start () {
		biggestValue = new Vector2(float.MinValue, float.MinValue);
		smallestValue = new Vector2(float.MaxValue, float.MaxValue);

		if (numbers != null && numbers.Count != 0) {
			numbers.Sort (delegate(Vector2 x, Vector2 y) {
				return x.y.CompareTo (y.y);
			});
			biggestValue.y = numbers [numbers.Count - 1].y;
			smallestValue.y = numbers [0].y;

			numbers.Sort (delegate(Vector2 x, Vector2 y) {
				return x.x.CompareTo (y.x);
			});
			biggestValue.x = numbers [numbers.Count - 1].x;
			smallestValue.x = numbers [0].x;
		}

		UpdateGraphic ();
	}
}

