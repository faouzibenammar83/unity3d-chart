﻿using UnityEngine;
using System.Collections;

public class TestingGraphicPlot : MonoBehaviour {

	public GameObject [] graphicsGO;
	public GameObject clipping;
	public Vector2 clip;

	private float x = -100; 

	void Start() {
		(clipping.GetComponent(typeof(GraphicBase)) as GraphicBase).ClipView(clip.x,clip.y);
		StartCoroutine(UpdateClippingView());
	}

	IEnumerator UpdateClippingView() {
		float offset = 10;
		while (true) {
			(clipping.GetComponent(typeof(GraphicBase)) as GraphicBase).ClipView(offset + clip.x, offset + clip.y);
			yield return new WaitForSeconds(1);
			offset += 2;
		}
	}

	void Update() {
		float y = Random.Range(-1000, 1000);
		foreach(GameObject go in graphicsGO) {
			Component [] graphics = go.GetComponents(typeof(GraphicBase));
			foreach(Component c in graphics) {
				GraphicBase graphic = (GraphicBase)c;
				graphic.AddCoordinate(new Vector2(x, y));
				graphic.UpdateGraphic();
			}
		}

		x++;
	}

}

